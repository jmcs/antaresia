"""
Antaresia is subset of Python to write configuration files.
"""
from .loaders import load, load_string

__all__ = ["load", "load_string"]
__version__ = "0.0.1a6"
